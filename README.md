# PokerShot

PokerShot is an Electron powered app made to Analyse, stream and save your poker games

## Installation

```
git clone https://gitlab.com/flavien.fechino/pokershot.git
npm install
npm start
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

(not currently defined)
