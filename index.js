const electron = require("electron");
const url = require("url");
const path = require("path");
const { app, BrowserWindow, Menu, BrowserView } = electron;
const {cardToJSON} = require("./src/js/utils")
const {calculateEquity} = require("poker-odds");
const { notDeepEqual } = require("assert");
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;

var exec = require('child_process').exec;
function execute(command, callback){
    exec(command, function(error, stdout, stderr){ callback(stdout); });
};

// Path consts
const src = "/src/";
const html = src + "html/";
const js = src + "js/";
const ressources = src + "ressources/";
const styles = src + "styles/";

// IPC
const ipc = electron.ipcMain;

// Window variables
let mainWindow;
let contentView;
let contentBounds;

// Electron Reload
require("electron-reload")(__dirname, {
  electron: path.join(__dirname, "node_modules", ".bin", "electron.cmd"),
});

// Executed upon app start
app.on("ready", function () {
  // Container of app, contains borderless window and side menu
  mainWindow = new BrowserWindow({
    title: "PokerShot",
    frame: false,
    minHeight: 600,
    minWidth: 650,
    icon: path.join(__dirname, ressources, "jeton.png"),
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
    },
  });
  mainWindow.setBackgroundColor("#36393F");
  mainWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, html, "mainWindow.html"),
      protocol: "file:",
      slashes: true,
    })
  );
  // Content of app itself
  contentView = new BrowserView({
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
    },
  });
  contentBounds = mainWindow.getContentBounds();
  mainWindow.setBrowserView(contentView);
  contentView.setBounds({
    x: 200,
    y: 30,
    width: contentBounds.width - 200,
    height: contentBounds.height - 30,
  });
  contentView.setAutoResize({ width: true, height: true });
  // First page loaded
  mainWindow.webContents.on("did-finish-load", function () {
    contentView.webContents.loadURL(
      url.format({
        pathname: path.join(__dirname, html, "welcomeWindow.html"),
        protocol: "file:",
        slashes: true,
      })
    );
  });
  // Build menu from template
  const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
  // Insert Menu
  Menu.setApplicationMenu(mainMenu);
  // Enable dev window REMOVE BEFORE RELEASE
  //mainWindow.webContents.openDevTools({detach:true});
  //contentView.webContents.openDevTools({detach: true});
});

// Listens for click in menuHandler.js to open welcomeWindow.html in the content web view
ipc.on("displayWelcome", (event, arg) => {
  contentView.webContents.loadURL(
    url.format({
      pathname: path.join(__dirname, html, "welcomeWindow.html"),
      protocol: "file:",
      slashes: true,
    })
  );
});

// Listens for click in menuHandler.js to open aboutWindow.html in the content web view
ipc.on("displayAbout", (event, arg) => {
  contentView.webContents.loadURL(
    url.format({
      pathname: path.join(__dirname, html, "aboutWindow.html"),
      protocol: "file:",
      slashes: true,
    })
  );
});

// Listens for click in menuHandler.js to open linkCfgWindow.html in the content web view
ipc.on("displayPlayer1", (event, arg) => {
  contentView.webContents.loadURL(
    url.format({
      pathname: path.join(__dirname, html, "player1.html"),
      protocol: "file:",
      slashes: true,
    })
  );
});

// Listens for click in menuHandler.js to open linkCfgWindow.html in the content web view
ipc.on("displayPlayer2", (event, arg) => {
  contentView.webContents.loadURL(
    url.format({
      pathname: path.join(__dirname, html, "player2.html"),
      protocol: "file:",
      slashes: true,
    })
  );
});

// Listens for click in menuHandler.js to open mappingWindow.html in the content web view
ipc.on("displayCFG", (event, arg) => {
  contentView.webContents.loadURL(
    url.format({
      pathname: path.join(__dirname, html, "mappingWindow.html"),
      protocol: "file:",
      slashes: true,
    })
  );
});

function sendCardDelay(i) {
  setTimeout(()=>{
   contentView.webContents.send("showCard",cardToJSON(board[i]));
  },2000+i*500);
}

var board = new Array(5);
board = []/*,
"Th",
"Qh"]*/
var nbCards = board.length;

ipc.on("addCard",(event,arg)=>{
  console.log(arg)
  board.push(arg);
  nbCards++;
  contentView.webContents.send("showCard",cardToJSON(arg));
})
ipc.on("updateOdds",(event,arg)=>{
  if(nbHand1==2 && nbHand2==2 && nbCards>2){
  var equity = calculateEquity(hand,board, 1000,true);
  var oddsP1 = (equity[0]["wins"]/equity[0]["count"]*100).toPrecision(3);
  var oddsP2 = (equity[1]["wins"]/equity[1]["count"]*100).toPrecision(3);
  var oddsDraw = (equity[0]["ties"]/equity[0]["count"]*100).toPrecision(3);
  console.log("proba de 1 de gagner: " + oddsP1 + "%");
  console.log(equity[0]["handChances"])
  console.log("proba de 2 de gagner: " + oddsP2 + "%");
  console.log("proba d'egaliser: " + oddsDraw + "%");
  console.log(equity[1]["handChances"])
  mainWindow.webContents.send("displayOdds",{"p1":oddsP1,"p2":oddsP2,"draw":oddsDraw})
  }
})
ipc.on("loadCards",(event,arg)=>{
  for(var i=0; i<nbCards;i++){
    sendCardDelay(i);
  }
  if(nbHand1==2 && nbHand2==2 && nbCards>2){
  var equity = calculateEquity(hand,board, 1000,true);
  var oddsP1 = (equity[0]["wins"]/equity[0]["count"]*100).toPrecision(3);
  var oddsP2 = (equity[1]["wins"]/equity[1]["count"]*100).toPrecision(3);
  var oddsDraw = (equity[0]["ties"]/equity[0]["count"]*100).toPrecision(3);
  console.log("proba de 1 de gagner: " + oddsP1 + "%");
  console.log(equity[0]["handChances"])
  console.log("proba de 2 de gagner: " + oddsP2 + "%");
  console.log("proba d'egaliser: " + oddsDraw + "%");
  console.log(equity[1]["handChances"])
  mainWindow.webContents.send("displayOdds",{"p1":oddsP1,"p2":oddsP2,"draw":oddsDraw})
  }
  //exec('activate & conda activate C:\\Users\\FlFe\\anaconda3\\envs\\downgrade & python ../DetectCards/final.py',
    //function (error, stdout, stderr) {
        //console.log('stdout: ' + stdout);
        //console.log('stderr: ' + stderr);
        //if (error !== null) {
             //console.log('exec error: ' + error);
        //}
    //});
  //execute('activate & conda activate C:\\Users\\FlFe\\anaconda3\\envs\\downgrade & python ../DetectCards/final.py',function(out){
    //console.log(out);
  //});

})

var hand = [[],[]];
var nbHand1=0;
var nbHand2=0;
ipc.on("addPlayer1Card",(event,arg)=>{
  if (nbHand1<2 && !(board.includes(arg))){
    hand[0][nbHand1] = arg;
    nbHand1++;
    console.log(hand);
  }
})
ipc.on("addPlayer2Card",(event,arg)=>{
  if( nbHand2<2 && !(board.includes(arg))){
    hand[1][nbHand2] = arg;
    nbHand2++;
    console.log(hand);
  }
})
ipc.on("analyseFrame",(event,arg)=>{
})

// Menu template
const mainMenuTemplate = [
  {
    label: "File",
    submenu: [
      {
        label: "Quit",
        accelerator: "CmdOrCtrl+Q",
        click() {
          app.quit();
        },
      },
    ],
  },
];
