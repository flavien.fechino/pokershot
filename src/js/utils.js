
module.exports = {
    cardToJSON: (cardName)=>{
    const n = cardName.length;
    const suit = cardName.slice(-1);
    var suitNb
    switch(suit){
        case "s":
            suitNb=0
            break;
        case "c":
            suitNb=2
            break;
        case "h":
            suitNb=1
            break;
        case "d":
            suitNb=3
            break;
    }
    var rank = cardName.slice(0,-1);
    switch(rank){
        case "A":
            rank = 1
            break;
        case "T":
            rank = 10
            break;
        case "J":
            rank = 11
            break;
        case "Q":
            rank = 12
            break;
        case "K":
            rank = 13
            break;
    }
    //console.log({"rank":rank, "suit": suitNb});
    return ({"rank":Number(rank), "suit": suitNb})
},
    JSONToCard: (JSONCard) =>{
        var suit = JSONCard["suit"];
        var rank = JSONCard["rank"];
        switch(rank){
            case 1:
                rank =  "A"
                break;
            case 10:
                rank = "T"
                break;
            case 11:
                rank = "J"
                break;
            case 12:
                rank = "Q"
                break;
            case 13:
                rank = "K"
                break;
        }
        switch(suit){
            case 0:
                suit = "s"
                break;
            case 1:
                suit = "h"
                break;
            case 2:
                suit = "c"
                break;
            case 3:
                suit = "d"
                break;
        }
        return rank.toString() + suit.toString();
    }
}