
const electron = require("electron");
const fs = require("fs");
const $ = require("jquery");
const { ipcRenderer, BrowserWindowProxy } = require("electron");
const apps= "../js/apps/";
//const ipc = electron.ipcRenderer;
const { remote } = electron;
const { dialog } = electron.remote;
const {cardToJSON} = require("../js/utils.js")
var win = remote.getCurrentWindow();

window.onload = init;

function init(){
var formP1 = document.getElementById("cardName1");
var formP2 = document.getElementById("cardName2");

if(formP1){
formP1.addEventListener("submit",function(event){
    event.preventDefault();
    ipcRenderer.send("addPlayer1Card",(document.getElementById("card").value));
},false);
}

if(formP2){
formP2.addEventListener("submit",function(event){
    event.preventDefault();
    ipcRenderer.send("addPlayer2Card",(document.getElementById("card").value));
},false);
}


}