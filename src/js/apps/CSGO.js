const { fsyncSync } = require("original-fs");

module.exports = {
  read: function (file){
    const regexBindFull = /\b(bind)\s+(.*)$/gm;
    var regexBindIterable = file.matchAll(regexBindFull);
    let binds = [];
    for (let bind of regexBindIterable) {
      binds.push(
        bind[2]
          .replace(/"/gm, "")
          .match(/^(\S+)\s(.*)/)
          .slice(1)
      );
    }
    return(JSON.parse(convertArrayToJSON(binds)).binds);
  },
  

  write: function (file, content){
    fileString = "";
    // creates csgo cfg file structure
    for (let element of content){
      fileString += "bind \"" + element['key'] + "\"  \"" + element['action'] + "\"\n";
    }
    fs.writeFile(file, fileString, function (err) {
      if (err) return console.log(err);
      console.log('Hello World > helloworld.txt');})
  }
}