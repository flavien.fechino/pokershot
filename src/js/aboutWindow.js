const electron = require("electron");
const $ = require("jquery");
const remote = electron.remote;

// Version const
const version = remote.app.getVersion();

// Adds version to sentence
$(function () {
  $("#version_sentence").append(" " + version);
});
