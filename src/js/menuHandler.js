const electron = require("electron");
const fs = require("fs");
const { isEmptyObject } = require("jquery");
const $ = require("jquery");
const { remote } = electron;
const { dialog } = electron.remote;

// IPC
const ipc = electron.ipcRenderer;

// Window
var win = remote.getCurrentWindow();

// Handle the titlebar to minimize the window
$(document).on("click", ".minimize", () => {
  win.minimize();
});

// Handle the titlebar to maximize the window
$(document).on("click", ".maximize", () => {
  if (!win.isMaximized()) {
    win.maximize();
  } else {
    win.unmaximize();
  }
});

// Handle the titlebar to close the window
$(document).on("click", ".close", () => {
  win.removeBrowserView(win.getBrowserView());
  win.close();
});

// Handle the menu content
$(document).on("click", ".changeCFG", () => {
  ipc.send("displayCFG");
});

$(document).on("click", ".welcome", () => {
  loadWelcomeContent();
});

$(document).on("click", ".player1", () => {
  loadPlayer1();
});

$(document).on("click", ".player2", () => {
  loadPlayer2();
});

$(document).on("click", ".about", () => {
  loadAboutContent();
});

// loadWelcomeContent : Displays the Welcome content in the BrowserView with welcomeWindow.html.
function loadWelcomeContent() {
  ipc.send("displayWelcome");
}

// loadLinkCfgContent : Displays the LinkCfg content in the BrowserView with linkCfgWindow.html.
function loadPlayer1() {
  ipc.send("displayPlayer1");
}

// loadLinkCfgContent : Displays the LinkCfg content in the BrowserView with linkCfgWindow.html.
function loadPlayer2() {
  ipc.send("displayPlayer2");
}

// loadAboutContent : Displays the About content in the BrowserView with aboutWindow.html.
function loadAboutContent() {
  ipc.send("displayAbout");
}

window.onload = init;
function init(){
var p1 = document.getElementById("p1");
var p2 = document.getElementById("p2");

ipc.on("displayOdds",(event,arg)=>{
  console.log(arg);
  p1.innerHTML  = arg["p1"] + "%";
  p2.innerHTML  = arg["p2"] + "%";
})
}