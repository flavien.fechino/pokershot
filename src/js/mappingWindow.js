const electron = require("electron");
const fs = require("fs");
const $ = require("jquery");
const { ipcRenderer, BrowserWindowProxy } = require("electron");
const apps= "../js/apps/";
//const ipc = electron.ipcRenderer;
const { remote } = electron;
const { dialog } = electron.remote;

var win = remote.getCurrentWindow();
var result;

var enabled = true;
var WebCamera = require("webcamjs");
WebCamera.attach('#camdemo');
var nbCards = 0;

document.getElementById("start").addEventListener('click',function(){
   if(!enabled){ // Start the camera !
     enabled = true;
     WebCamera.attach('#camdemo');
     console.log("The camera has been started");
   }else{ // Disable the camera !
      WebCamera.snap(function(data_uri){
        console.log(data_uri);
        downloadURI(data_uri,"test.png");
      })
     enabled = false;
     WebCamera.reset();
    console.log("The camera has been disabled");
   }
},false);

function downloadURI(uri, name) {
  var link = document.createElement("a");
  link.download = name;
  link.href = uri;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
  delete link;
}
var $container = document.getElementById('cards_container');

// create Deck
var deck = Deck();

// add to DOM
deck.mount($container);
//deck.addModule(poker)
deck.cards.forEach(function (card, i) {
    //card.disableDragging();
    //card.disableDragging();
  card.animateTo({
    delay: 1000 + i * 2, // wait 1 second + i * 2 ms
    duration: 500,
    ease: 'quartOut',

    x: window.innerWidth - 3*window.innerWidth / 4,
    y: window.innerHeight - window.innerHeight / 8
  })
});
ipcRenderer.send("loadCards","");
ipcRenderer.on("showCard",(event,arg)=>{
  //console.log(arg);
  deck.cards[nbCards].rank = arg["rank"];
  deck.cards[nbCards].suit = arg["suit"];
  //console.log(deck.cards[nbCards].rank);
  //console.log(deck.cards[nbCards].suit);
  deck.cards[nbCards].setSide('front')
  deck.cards[nbCards].animateTo({
    delay: 0,
    duration: 500,
    ease: 'quartOut',
    
    x: window.innerWidth - 3*window.innerWidth / 4 + (nbCards+1)*70,
    y: window.innerHeight - window.innerHeight / 8
  })
  nbCards++;
});


function search(ele) {
    if(event.key === 'Enter') {
        ipcRenderer.send("addCard",ele.value);
        ipcRenderer.send("updateOdds")    
    }
}